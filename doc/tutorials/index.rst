.. _Tutorials:

Tutorials
=========

..
    SPDX-License-Identifier: CC-BY-SA-4.0
    Copyright Tumult Labs 2023

The following tutorials introduce the key pieces of the Analytics API,
and help you get started making differentially private queries.

.. toctree::
   :maxdepth: 1

   first-steps
   privacy-budget-basics
   clamping-bounds
   groupby-queries
   simple-transformations
